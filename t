TUTORIAL on ho explica tot: https://www.youtube.com/watch?v=Bo0guUbL5uo 

el profiler, per veure informació guai: composer require profiler i composer require web-profiler-bundle
per engegar el servidor: symfony server:start
per veure les rutes del projecte: php bin/console debug:router
per crear un projecte: composer create-project symfony/skeleton blog
per crear els templates: composer require template --> per posar el boostrap a tot arreu a config/packages/twig.yaml posar-> form_theme: 'bootstrap_4_layout.html.twig'
per treballar al model: composer require orm
per crear entitats: php console make:entity --> va demanant la informació per a generar una classe amb els atributs (columnes) + getters/setters.
	- Això s'anomena Schema i per actualitzar-lo es fa ús de la comanda (es x quan ja s'està utilitzant): 
		php bin/console doctrine:schema:update --force <-- el del tutorial fa anar aquesta al principi ja...
	- Per crear-lo es fa ús de la comanda:
		php bin/console doctrine:schema:create 

per crear un controlador necessitem primer tenir el composer amb el make, per fer això es fa: composer require make i tmb executar composer require annotations.
	Ara podem executar: php bin/console make:controller <Nom controlador>





2 formes de crear formularis:
	-> a través del <form></form> de tota la vida
	-> a través de symfony... si es fa aixi, cal composer require form validator
		per a crear un post llavors simplement cal executar: php bin/console make:form
		el nom del formulari es XXXXXXType i despres dius a quina de les entitats modifica



--------------------------------------------------------------------------------------------------------------------------

Per a crear un registre/login ... d'usuaris, es fa:
	->php bin/console make:auth, pero requereix d'una classe "Usuari" que s'ha de crear, es crea a través de php bin/console make:user
	-> Migrar a la base de dades: php bin/console doctrine:schema:update --force  	(si es vol veure el codi sql q s'executarà es pot veure aixi: php bin/console doctrine:schema:update --dump-sql
