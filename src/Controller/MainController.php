<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        //aquest codi s'executa en funcio de la ruta que posem dins del ""
        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/custom/{name?}", name="custom")
     * @param Request $request
     * @return Response
     */
    public function custom(Request $request){
        // lo que hi ha dins de {} es accessible dins de la funcio. Si posem un ? es com que es opcional
        // Si fem dump($request) veurem tota la informacio de la request a la pantalla del navegador
        // per treballar amb templates cal executar: composer require template --> aixo genera una carpeta anomenada templates
        // per treballar amb la db: composer require orm ---> 1:09:24 https://www.youtube.com/watch?v=Bo0guUbL5uo
        $name = $request->get('name');
        $pp = 'aaa';
        return $this->render('home/custom.html.twig', ['name'=>$name, 'e'=>$pp]);
    }
}
