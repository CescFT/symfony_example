<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/post", name="post.")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param PostRepository $postRepository
     * @return Response
     */
    public function index(PostRepository $postRepository)
    {
        $posts = $postRepository -> findAll();

        return $this->render('post/index.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return Response
     */
    public function create (Request $request){
        // crear un nou post amb un titol
        $post = new Post();
        $post->setTitle('This is going to be a title');

        // entity manager --> connecta amb la db
        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();

        return $this->redirect($this->generateUrl('post.index'));
    }

    /**
     * @Route("/createWithSymfonyForm", name="createWithSymfonyForm")
     * @param Request $request
     * @return Response
     */
    public function createWithForm(Request $request){
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);
        //$form->getErrors(); //--> treiem tots els errors que han passat al formulari
        if($form->isSubmitted()){ //&& $form->isValid() --> aixo es per la validacio del assert que hi ha colocat al post.php
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            return $this->redirect($this->generateUrl('post.index'));
        }

        return $this->render('post/createSymfonyPost.html.twig',[
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/show/{id}", name="show")
     * @return Response
     */
    public function show($id, PostRepository $postRepository){
        //passant el $id, PostRepository $postRepository com a parametres i fent aquest codi, funciona
        // el paio del tutorial li passa com a parametre de la funcio el Post directament i li va, a mi no aixi q no ho faig.
        $post = $postRepository->find($id);
        return $this->render('post/show.html.twig', [
            'post'=> $post
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @return Response
     */
    public function remove($id, PostRepository $postRepository){
        $post = $postRepository->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        $this->addFlash('success', 'Your post was removed');

        return $this->redirect($this->generateUrl('post.index'));
    }
}
